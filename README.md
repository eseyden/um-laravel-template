# UMT Templating package

## Installation

    Add to your composer.json:
        "require": {
            "umt/template": "dev-master"
        }

    Run 'php artisan config:publish umt/template' to publish local configs


	Edit app/start/artisan.php, add
		Artisan::add(new UMT\Template\UpdateTemplateCommand);

    Update app/config/app.php with:
        "providers" => array(
            ...
            "UMT\Template\TemplateServiceProvider",
        ),

    edit app/packages/UMT/template/config.php to suit.

## Usage


### Edit app/Routes.php
	Route::get('/', 'HomeController@index');

### Edit app/controller/HomeController.php
	public function index()
	{
		return View::of($this->template, $this->data['template'])->nest('content', 'hello', $this->data);

	}

### Edit app/controller/BaseController.php
	class BaseController extends UMT\Template\UMTController {
