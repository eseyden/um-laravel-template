<?php

return array(

	/*
	|--------------------------------------------------------------------------
	| Grouper Base URL
	|--------------------------------------------------------------------------
	*/

	'application_less_file' => "applications/um-faculty-awards",
	'application_template_file' =>
		array(
			"master" => "http://staging.umt.edu/templates/places/template.php",
			"full" => "http://staging.umt.edu/templates/places/fullscreen.php"
		)
);
