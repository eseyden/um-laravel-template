<?php


namespace UMT\Template;

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class UpdateTemplateCommand extends Command
{

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'template:update';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Command description.';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$templates = \Config::get('template::application_template_file');
		if (!is_array($templates)) {
			$templates = array("master" => $templates);
		}
		foreach ($templates as $key => $url) {

			$template = implode(array_map(function ($item) {
				return $item;
			}, file($url)));

			\File::put('app/views/layouts/' . $key . '.blade.php', $template); //
			print("writting ".$key." \n");
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array( //array('example', InputArgument::REQUIRED, 'An example argument.'),
		);
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array( //array('example', null, InputOption::VALUE_OPTIONAL, 'An example option.', null),
		);
	}

}