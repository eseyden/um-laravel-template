<?php

namespace UMT\Template;
class UMTController extends \Controller {

	protected $layout = 'layouts.master';
	public $data = array("template"=>array("title"=>"", "content"=>""));
	public $template = "layout";
	/**
	 * Setup the layout used by the controller.
	 *
	 * @return void
	 */
	protected function setupLayout()
	{
		if ( ! is_null($this->layout))
		{
			$this->layout = View::make($this->layout);
		}
	}

}